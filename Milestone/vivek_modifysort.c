#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include<math.h>
int integer_compare(const void *a,const void *b) 
{
	int *x=(int *)a;
	int *y=(int *)b;
	return *x-*y;
}
int str_compare(const void *a,const void *b) 
{
	return (strcmp((char *)a,(char *)b));
}
int double_compare(const void *a,const void *b) 
{
	double *x = (double *)a;
	double *y = (double *)b;
	if (*x<*y)
	{
		return -1;
	}
	else if (*x>*y) 
	{
		return 1; 
	}
	else
	{
		return 0;
	}
		
}
int absolute_compare(const void *a,const void *b) 
{
	int *x = (int *)a;
	int *y = (int *)b;
	if(abs(*x)>abs(*y))
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

int main()
{
	int i;
	int num_arr[5]={20,10,5,30,1};
	char str_arr[6]={'j','a','i','c','s','\0'};
	double dob_arr[5] = {1000.0, 2.0, 3.4, 7.0, 50.0};
	int abs_arr[5]={-30,1,35,-2,5};
	qsort(num_arr,5,sizeof(int),integer_compare);
	qsort(str_arr,6,sizeof(char),str_compare);
	qsort(dob_arr,5,sizeof(double),double_compare);
	qsort(abs_arr,5,sizeof(int),absolute_compare);
	printf("\nThe sorted integer array is: \n\n");
	for(i=0;i<5;i++)
	{
		printf("%d\t",num_arr[i]);
	}
	printf("\n\nThe sorted string array is: \n\n");
	for(i=0;i<6;i++)
	{
		printf("%c\t",str_arr[i]);
	}
	printf("\n\nThe sorted double array is: \n\n");
	for(i=0;i<5;i++)
	{
		printf("%lf\t",dob_arr[i]);
	}
	printf("\n\nThe sorted integer array w.r.t.absolute value is: \n\n");
	for(i=0;i<5;i++)
	{
		printf("%d\t",abs_arr[i]);
	}	
	printf("\n");
}
